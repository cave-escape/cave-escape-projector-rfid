#include "raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)devices;
    (void)params;
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;
    char rfid[24];
    if (devices.pFilmPresent->check_low_and_clear())
    {
        uint8_t rfid_length = devices.pRFIDReader->get(rfid);
        if (rfid_length)
        {
            params.pRFID->set(rfid);
            raat_logln(LOG_APP, "Detected film reel %s", rfid);
        }
        else
        {
            raat_logln(LOG_APP, "IR trigger but no reel detected");    
        }
    }

    if (devices.pFilmPresent->check_high_and_clear())
    {
        params.pRFID->set("");
        raat_logln(LOG_APP, "Film reel removed");
        devices.pRFIDReader->forget();
    }
}
